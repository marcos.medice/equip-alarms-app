import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [

  {
    title: 'CONTROLE',
    group: true,
  },
  {
    title: 'Gerenciamento',
    icon: 'settings-2-outline',
    children: [
      {
        title: 'Equipamentos',
        link: '/pages/management/equipments',
      },
      {
        title: 'Alarmes',
        link: '/pages/management/alarms',
      },
      {
        title: 'Eventos',
        link: '/pages/management/events',
      }
    ]
  }
];
