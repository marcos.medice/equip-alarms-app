import { Injectable } from '@angular/core';
import { RestService } from 'app/commons/services/rest.service';
import { environment } from 'environments/environment';
import { RestHeadersService } from 'app/commons/services/rest-headers.service';
import { Observable } from 'rxjs';
import { Alarm } from '../model/alarm.model';

@Injectable({
    providedIn: 'root'
})
export class AlarmService {

    constructor(private restService: RestService, private headerService: RestHeadersService) { }

    public getAllAlarms(): Observable<Array<Alarm>> {
        const headers = this.headerService.getHeaders();
        return this.restService.get(environment.api.alarms, headers);
    }

    public get(equipmentId: String): Observable<any> {
        const headers = this.headerService.getHeaders();
        let query = `&_equipmentId=${equipmentId}`;
        return this.restService.get(`${environment.api.alarms}?${query}`, headers);
    }
    
    public saveAlarm(alarm: Alarm): Observable<any> {
        const headers = this.headerService.getHeaders();
        return this.restService.post(environment.api.alarms, alarm, headers);
    }

    public deleteAlarm(alarm: Alarm): Observable<Array<any>> {
        const headers = this.headerService.getHeaders();
        return this.restService.delete(`${environment.api.alarms}/${alarm._id}`, headers);
    }

}