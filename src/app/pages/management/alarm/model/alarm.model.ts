export class Alarm {
  _id: string;
  description: string;
  classification: string;
  _equipmentId: string;
  creationDate: Date;
  lastUpdate: Date;
}