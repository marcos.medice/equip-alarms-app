import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EquipmentService } from '../equipment/service/equipment.service';
import { Equipment } from '../equipment/model/equipment.model';
import { Alarm } from './model/alarm.model';
import { AlarmService } from './service/alarm.service';

@Component({
  selector: 'ngx-alarm',
  templateUrl: 'alarm.component.html',
  styleUrls: ['alarm.component.scss'],
})
export class AlarmComponent implements OnInit {

  public alarms: Array<Alarm>;
  public equipments: Array<Equipment>;

  public addAlarmForm: FormGroup;

  public classifications = ['Baixa', 'Média', 'Alta'];

  constructor(private alarmService: AlarmService, 
      private formBuilder: FormBuilder,
      private equipmentService: EquipmentService) {}

  public ngOnInit(): void {
    this.buildForm();
    this.load();
  }

  public async onSubmit() {

    if (this.addAlarmForm.valid) {
      this.addAlarmForm.disable();

      let alarm = new Alarm();

      alarm.description = this.addAlarmForm.value.description;
      alarm.classification = this.addAlarmForm.value.classification;
      alarm._equipmentId = this.addAlarmForm.value._equipmentId;

      this.alarmService.saveAlarm(alarm).subscribe(
        (data: any) => {
          this.loadAlarm();
          this.addAlarmForm.enable();
          this.addAlarmForm.reset();
        }, error => {
          // mensagem
          this.addAlarmForm.enable();
          this.addAlarmForm.reset();
        });

    }

  }

  public delete(alarm: Alarm): void {

    this.alarmService.deleteAlarm(alarm).subscribe(
      (data: any) => {
        this.load();
      }, error => {

      });
  }

  private loadEquipment(): void {
    this.equipmentService.getAllEquipments().subscribe(
      (data: any) => {
        this.equipments = data.data;
      });
  }

  private loadAlarm(): void {
    this.alarmService.getAllAlarms().subscribe(
      (data: any) => {
        this.alarms = data.data;
      });
  }

  private load(): void {
    this.loadAlarm();
    this.loadEquipment();
  }

  private buildForm(): void {

    this.addAlarmForm = this.formBuilder.group({
      description: ['', [Validators.required]],
      classification: ['', [Validators.required]],
      _equipmentId: ['', [Validators.required]]
    });
  }

}
