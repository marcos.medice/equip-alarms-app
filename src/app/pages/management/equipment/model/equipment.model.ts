export class Equipment {
  _id: String;
  name: String;
  serialNumber: String;
  equipType: {
      tensao: String,
      corrente: String,
      oleo: String
  };
  creationDate: Date;
  lastUpdate: Date;
}