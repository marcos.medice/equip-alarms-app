import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Equipment } from './model/equipment.model';
import { EquipmentService } from './service/equipment.service';

@Component({
  selector: 'ngx-equipment',
  templateUrl: 'equipment.component.html',
  styleUrls: ['equipment.component.scss'],
})
export class EquipmentComponent implements OnInit {

  public equipments: Array<Equipment>;

  public addEquipmentForm: FormGroup;

  constructor(private equipmentService: EquipmentService, 
      private formBuilder: FormBuilder) {}

  public ngOnInit(): void {
    this.buildForm();
    this.loadEquipments();
    this.onSubmit();
  }

  public async onSubmit() {

    if (this.addEquipmentForm.valid) {
      this.addEquipmentForm.disable();

      let equipment = new Equipment();
      console.log(this.addEquipmentForm.value);

      equipment.name = this.addEquipmentForm.value.name;
      equipment.serialNumber = this.addEquipmentForm.value.serialNumber;
      equipment.equipType.tensao = this.addEquipmentForm.value.equipTypeTensao;
      equipment.equipType.corrente = this.addEquipmentForm.value.equipTypeCorrente;
      equipment.equipType.oleo = this.addEquipmentForm.value.equipTypeOleo;

      this.equipmentService.saveEquipment(equipment).subscribe(
        (data: any) => {
          this.loadEquipments();
          this.addEquipmentForm.enable();
          this.addEquipmentForm.reset();
        }, error => {
          // mensagem
          this.addEquipmentForm.enable();
          this.addEquipmentForm.reset();
        });
    }

  }

  public delete(equipment: Equipment): void {

    this.equipmentService.deleteEquipment(equipment).subscribe(
      (data: any) => {
        this.loadEquipments();
      }, error => {

      });
  }
  
  private loadEquipments(): void {
    this.equipmentService.getAllEquipments().subscribe(
      (data: any) => {
        this.equipments = data.data;
      });
  }

  private buildForm(): void {

    this.addEquipmentForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      serialNumber: ['', [Validators.required]],
      equipTypeTensao: ['', [Validators.required]],
      equipTypeCorrente: ['', [Validators.required]],
      equipTypeOleo: ['', [Validators.required]]
    });
  }

}
