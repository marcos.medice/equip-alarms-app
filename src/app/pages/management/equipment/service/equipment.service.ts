import { Injectable } from '@angular/core';
import { RestService } from 'app/commons/services/rest.service';
import { environment } from 'environments/environment';
import { RestHeadersService } from 'app/commons/services/rest-headers.service';
import { Observable } from 'rxjs';
import { Equipment } from '../model/equipment.model';

@Injectable({
    providedIn: 'root'
})
export class EquipmentService {

    constructor(private restService: RestService, private headerService: RestHeadersService) { }

    public getAllEquipments(): Observable<Array<Equipment>> {
        const headers = this.headerService.getHeaders();
        return this.restService.get(environment.api.equipments, headers);
    }
    
    public saveEquipment(equipment: Equipment): Observable<any> {
        const headers = this.headerService.getHeaders();
        return this.restService.post(environment.api.equipments, equipment, headers);
    }

    public deleteEquipment(equipment: Equipment): Observable<Array<any>> {
        const headers = this.headerService.getHeaders();
        return this.restService.delete(`${environment.api.equipments}/${equipment._id}`, headers);
    }


}