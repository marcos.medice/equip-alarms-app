import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManagementComponent } from './management.component';
import { EquipmentComponent } from './equipment/equipment.component';
import { AlarmComponent } from './alarm/alarm.component';
import { EventComponent } from './event/event.component';

const routes: Routes = [{
  path: '',
  component: ManagementComponent,
  children: [
    {
      path: 'equipments',
      component: EquipmentComponent,
    },
    {
      path: 'alarms',
      component: AlarmComponent,
    },
    {
      path: 'events',
      component: EventComponent,
    }

  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManagementRoutingModule {
}
