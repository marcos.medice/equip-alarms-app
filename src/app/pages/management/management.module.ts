import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  NbAccordionModule,
  NbButtonModule,
  NbCardModule,
  NbListModule,
  NbRouteTabsetModule,
  NbStepperModule,
  NbTabsetModule,
  NbUserModule,
  NbCheckboxModule,
  NbInputModule,
  NbActionsModule,
  NbDatepickerModule,
  NbIconModule,
  NbRadioModule,
  NbSelectModule,
} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { ManagementRoutingModule } from './management-routing.module';
import { ManagementComponent } from './management.component';
import { NewsService } from './news.service';
import { FormsModule as ngFormsModule } from '@angular/forms';
import { ImageCropperModule } from 'ngx-image-cropper';
import { EquipmentComponent } from './equipment/equipment.component';
import { AlarmComponent } from './alarm/alarm.component';
import { EventComponent } from './event/event.component';



@NgModule({
  imports: [
    ThemeModule,
    ngFormsModule,
    ReactiveFormsModule,
    NbCheckboxModule,
    NbInputModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    NbStepperModule,
    NbCardModule,
    NbButtonModule,
    NbListModule,
    NbAccordionModule,
    NbUserModule,
    ManagementRoutingModule,
    NbActionsModule,
    NbDatepickerModule,
    NbIconModule,
    NbRadioModule,
    NbSelectModule,
    ImageCropperModule,
  ],
  declarations: [
    EquipmentComponent,
    AlarmComponent,
    EventComponent,
    ManagementComponent
  ],
  providers: [
    NewsService,
  ],
})
export class ManagementModule { }
