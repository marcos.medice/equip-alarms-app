import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EquipmentService } from '../equipment/service/equipment.service';
import { Equipment } from '../equipment/model/equipment.model';
import { Event } from './model/event.model';
import { EventService } from './service/event.service';
import { AlarmService } from '../alarm/service/alarm.service';
import { Alarm } from '../alarm/model/alarm.model';

@Component({
  selector: 'ngx-event',
  templateUrl: 'event.component.html',
  styleUrls: ['event.component.scss'],
})
export class EventComponent implements OnInit {

  public events: Array<Event>;
  public equipments: Array<Equipment>;
  public alarms: Array<Alarm>;
  public alarmsList: Array<Alarm>;
  public selectedEquipment: Equipment;

  public addEventForm: FormGroup;
  public activateEventForm: FormGroup;

  constructor(private eventService: EventService,
    private equipmentService: EquipmentService,
    private alarmService: AlarmService,
    private formBuilder: FormBuilder) {}

  public ngOnInit(): void {
    this.buildForm();
    this.load();
  }

  public async onSubmit() {

    if (this.addEventForm.valid) {
      this.addEventForm.disable();

      let event = new Event();

      event._alarmId = this.addEventForm.value._alarmId;

      console.log(event._alarmId);

      this.eventService.saveEvent(event).subscribe(
        (data: any) => {
          this.load();
          this.addEventForm.enable();
          this.addEventForm.reset();
        }, error => {
          // mensagem
          this.addEventForm.enable();
          this.addEventForm.reset();
        });
    }
  }

  private loadEquipment(): void {
    this.equipmentService.getAllEquipments().subscribe(
      (data: any) => {
        this.equipments = data.data;
      });
  }

  private loadAlarm(): void {
    this.alarmService.getAllAlarms().subscribe(
      (data: any) => {
        this.alarms = data.data;
      });
  }

  public loadAlarmsByEquipmentId(event): void {
    this.selectedEquipment = this.addEventForm.value.equipment;
    this.alarmService.get(this.selectedEquipment._id).subscribe(
      (data: any) => {
        this.alarmsList = data.data;
      });
  }

  private loadEvent(): void {
    this.eventService.getAllEventsByStatus().subscribe(
      (data: any) => {
        this.events = data.data;
      });
  }

  public toggleStatus(event: Event) {
    this.eventService.putEvent(event).subscribe(
      (data: Event) => {
        this.load();
      }, error => {

      });
  }

  private load(): void {
    this.loadEvent();
    this.loadAlarm();
    this.loadEquipment();
  }

  private buildForm(): void {

    this.addEventForm = this.formBuilder.group({
      _alarmId: ['', [Validators.required]],
      equipment: ['', [Validators.required]]
    });
  }

}