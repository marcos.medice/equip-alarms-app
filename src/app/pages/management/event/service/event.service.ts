import { Injectable } from '@angular/core';
import { RestService } from 'app/commons/services/rest.service';
import { environment } from 'environments/environment';
import { RestHeadersService } from 'app/commons/services/rest-headers.service';
import { Observable } from 'rxjs';
import { Event } from '../model/event.model';

@Injectable({
    providedIn: 'root'
})
export class EventService {

    constructor(private restService: RestService, private headerService: RestHeadersService) { }

    public getAllEventsByStatus(): Observable<Array<Event>> {
        const headers = this.headerService.getHeaders();
        const query = `&status=${true}`;
        return this.restService.get(`${environment.api.events}?${query}`, headers);
    }
    
    public saveEvent(event: Event): Observable<any> {
        const headers = this.headerService.getHeaders();
        return this.restService.post(environment.api.events, event, headers);
    }

    public putEvent(event: Event): Observable<any> {
        const headers = this.headerService.getHeaders();
        return this.restService.put(environment.api.events, event, headers);
    }

}