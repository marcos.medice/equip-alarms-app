export class Event {
  _id: string;
  _alarmId: string;
  activationDate: Date;
  deactivationDate: Date;
  status: boolean;
}