import { Component } from '@angular/core';

@Component({
  selector: 'ngx-management',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class ManagementComponent {
}
