import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }

  public getExternal(path: string, headers?: HttpHeaders, parameters?: any): Observable<any> {
    return this.http.get(`${path}`, { headers: headers, params: parameters });
  }

  public get(path: string, headers?: HttpHeaders, parameters?: any): Observable<any> {
    return this.http.get(`${environment.api.base}${path}`, { headers: headers, params: parameters })
      .pipe(catchError(this.handleError));
  }

  public post(path: string, object: any, headers?: HttpHeaders): Observable<any> {
    return this.http.post(`${environment.api.base}${path}`, object, { headers: headers })
      .pipe(catchError(this.handleError));
  }

  public put(path: string, object: any, headers?: HttpHeaders): Observable<any> {
    return this.http.put(`${environment.api.base}${path}`, object, { headers: headers })
      .pipe(catchError(this.handleError));
  }

  public patch(path: string, object: any, headers?: HttpHeaders): Observable<any> {
    return this.http.patch(`${environment.api.base}${path}`, object, { headers: headers })
      .pipe(catchError(this.handleError));
  }

  public delete(path: string, headers?: HttpHeaders): Observable<any> {
    return this.http.delete(`${environment.api.base}${path}`, { headers: headers }).pipe(catchError(this.handleError));
  }

  handleError(error) {
    return throwError(error.error);
  }

}
