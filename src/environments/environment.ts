export const environment = {
  production: false,
  api: {
    base: 'http://localhost:3000',
    equipments: '/api/v1/equipments',
    alarms: '/api/v1/alarms',
    events: '/api/v1/events'
  },
};
